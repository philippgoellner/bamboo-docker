Bamboo is an on-premises source code management solution for Git that's secure, fast, and enterprise grade. Create and manage repositories, set up fine-grained permissions, and collaborate on code – all with the flexibility of your servers.

Learn more about Stash: <https://www.atlassian.com/software/stash>

# Overview

This Docker container makes it easy to get an instance of Bamboo up and running
for evaluative purposes. Atlassian is not yet able to provide support for using Docker in production.

# Quick Start

For the `BAMBOO_HOME` directory that is used to store the repository data
(amongst other things) we recommend mounting a host directory as a [data volume](https://docs.docker.com/userguide/dockervolumes/#mount-a-host-directory-as-a-data-volume):

Set permissions for the data directory so that the runuser can write to it:

    $> docker run -u root -v /data/stash:/var/atlassian/application-data/bamboo atlassian/bamboo chown -R daemon  /var/atlassian/application-data/bamboo
