#!/bin/sh
mkdir -p ${BAMBOO_HOME}
chown -R ${RUN_USER}: ${BAMBOO_HOME}

# Fetch backup data over the network
if [ "${BACKUP_HOST}" != "false" ] && [ ! -f ${BAMBOO_HOME}/DONTSYNC ]; then
    chmod 400 ${BACKUP_KEY_FILE}
    echo "Start restoration of backup data over the network from ${BACKUP_HOST}:"
    rsync -arczv -e "ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${BACKUP_KEY_FILE}" ${BACKUP_USER}@${BACKUP_HOST}:${BACKUP_PATH}/* ${BAMBOO_HOME}
    if [ $? -eq 0 ]; then
        touch ${BAMBOO_HOME}/DONTSYNC
        echo "Successfully restored backup data over the network from ${BACKUP_HOST}"
        su -p -s /bin/bash -c "${BAMBOO_INSTALL}/bin/configure-and-start.sh" ${RUN_USER}
    else
        echo "Restoration of backup data over the network from ${BACKUP_HOST} failed"
    fi
else
    su -p -s /bin/bash -c "${BAMBOO_INSTALL}/bin/configure-and-start.sh" ${RUN_USER}
fi
